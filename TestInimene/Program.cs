﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Klassid2;

namespace TestInimene
{

    class Inim
    {
        private string _EesNimi;  // private asjad _nimega
        private string _PereNimi;
        private string _TäisNimi;

        public Inim(string eesNimi, string pereNimi)
            // lokaalsed muutujad ja parameetrid väikese tähega
        {
            this.EesNimi = eesNimi;
            this.PereNimi = pereNimi;
            _TäisNimi = this.EesNimi + " " + PereNimi;
        }


        public string EesNimi  // public asjad suure tähega
        {
            get { return _EesNimi; }
            set { _EesNimi = 
                    value.Substring(0, 1).ToUpper() + 
                    value.Substring(1, value.Length - 1).ToLower();
                _TäisNimi = _EesNimi + " " + _PereNimi;
            }


        }

        public string PereNimi
        {
            get { return _PereNimi; }
            set
            {
                _PereNimi =
                  value.Substring(0, 1).ToUpper() +
                  value.Substring(1, value.Length - 1).ToLower();
                _TäisNimi = _EesNimi + " " + _PereNimi;
            }

        }

        public string TäisNimi
        {
            get { return _TäisNimi; }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Inimene i1 = Inimene.CreateInimene("Ants");
            Inimene i2 = Inimene.CreateInimene("Malle");
            i1.Abielu(i2).TeeLaps("Jaak");
            i1.TeeLaps("Mati");


            i1.Lahutus().Abielu(Inimene.CreateInimene("Ülle"));
            i1.TeeLaps("Juta");

            foreach(var x in Inimene.Inimesed)
            {
                Console.WriteLine(x);
                Console.WriteLine("tema vanemad:");
                foreach (var v in x.Vanemad)
                    Console.WriteLine($"\t{v}");
                Console.WriteLine("tema lapsed:");
                foreach (var v in x.Lapsed)
                    Console.WriteLine($"\t{v}");

            }

        }
    }
}
