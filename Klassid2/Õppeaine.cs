﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klassid2
{
    class Õppeaine
    {
        String Teema;
        String Õpetaja;
        int AineMaht; // tundides
        int AinePunkte; // 
        bool AvE; // arvestus (f) või eksam (t)

        public Õppeaine(string Teema, int AineMaht, int AinePunkte)
        {
            this.Teema = Teema;
            this.AineMaht = AineMaht;
            this.AinePunkte = AinePunkte;
        }
        public Õppeaine(string teema, int maht, int punkte, string õpetaja)
            : this (teema, maht, punkte)
        {
            this.Õpetaja = õpetaja;
        }


    }
}
