﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klassid2
{
    public class Inimene
    {
        #region Staatilised asjad
        // klassi liikmed staatilised

        private static int _InimesteArv = 0;
        static int VastsündinuteArv = 0;
        static Dictionary<String,Inimene> _Inimesed = new Dictionary<string, Inimene>();

        internal static int GetInimesteArv() { return _InimesteArv; }

        public static Inimene CreateInimene(string nimi)
        {
            Inimene x;
            if (_Inimesed.TryGetValue(nimi, out x)) return x;
            else return new Inimene(nimi);
        }

        public static Inimene CreateInimene(String nimi, DateTime sünniAeg)
        {
            Inimene x = CreateInimene(nimi);
            x.SünniAeg = sünniAeg;
            return x;
        }

        public static int InimesteArv
        {
            get { return _InimesteArv; }
        }

        public static IEnumerable<Inimene> Inimesed
        {
            get { return _Inimesed.Values; }
        } 
        #endregion

        // klassi liikmed - instantsi

        // 1. andmeväljad e muutujad
        // private - nähtav vaid klassi sees
        // public - nähtav "igal pool"
        // internal - nähtav "projekti" sees (assembly sees)
        private string _Nimi;
        DateTime _SünniAeg;
        public bool? Sugu;

        public static void Abielu(Inimene i1, Inimene i2)
        {
            if (i1 != i2 && i1.partner == null && i2.partner == null)
            {
                i1.partner = i2;
                i2.partner = i1;
            }
        }

        public Inimene Abielu(Inimene x)
        {
            Inimene.Abielu(this, x);
            return this;
        }

        public Inimene Lahutus()
        {
            if (this.partner != null)
            {
                this.partner.partner = null;
                this.partner = null;
            }
            return this;
        }

        public List<Inimene> Vanemad = new List<Inimene>();
        public List<Inimene> Lapsed = new List<Inimene>();
        Inimene partner = null;


        // 2. funktsionaalsus e funktsioonid-meetodid


        // konstruktor - meetod new()
        // parameetritega või ilma
        private Inimene(string Nimi, DateTime sünniAeg)
        {
            this
                .SetNimi(Nimi)
                .SetSünniaeg(sünniAeg);
            _InimesteArv++;
            _Inimesed.Add(this.Nimi,this);
        }

        // overloeded construktor ja konstruktorite sidumine
        private Inimene(string Nimi) : this(Nimi, DateTime.Now)
        {
            VastsündinuteArv++;
        }

        public Inimene TeeLaps(string nimi)
        {
            if (this.partner != null)
                return TeeLaps(nimi, this.partner);
            else return null;
        }

        public Inimene TeeLaps(string Nimi, Inimene kellega)
        {
                Inimene x = new Inimene(Nimi, DateTime.Now);
                x.Vanemad.Add(this);
                x.Vanemad.Add(kellega);
                this.Lapsed.Add(x);
                kellega.Lapsed.Add(x);
                return x;
            
           
        }
        public DateTime GetSünniaeg()
        {
            return this.SünniAeg;
        }


        // get- ja set-meetodid
        public string GetNimi() { return this._Nimi; }

        public Inimene SetNimi(String uusNimi)
        {
            this._Nimi = uusNimi.Substring(0, 1).ToUpper() +
                        uusNimi.Substring(1, uusNimi.Length - 1).ToLower();
            return this; // meetodil võiks olla tagastus this!
        }

        public Inimene SetSünniaeg(DateTime uusSünniaeg)
        {
            if (uusSünniaeg <= DateTime.Now)
                this._SünniAeg = uusSünniaeg;
            return this;
        }

        // property on nagu meetod ja nagu field
        public string Nimi
        {
            get { return _Nimi; }
            set { SetNimi(value); }
        }

        public DateTime SünniAeg
        {
            get { return this._SünniAeg; }
            set { if (value < DateTime.Now) this._SünniAeg = value; }

        }

        public int Vanus
        {
            get { return (int)(Math.Floor((DateTime.Now - this._SünniAeg).TotalDays / 365.25)); }
        }

        // need kaks on näidis-propertid
        public string LinnKusElab
        { get; set; }
        // esimene on ilma ilmutatud muutujata

        private string _LinnKusEiEla;
        public string LinnKusEiEla
        {
            get { return _LinnKusEiEla; }
            set { _LinnKusEiEla = value; }
        }
        // teine on ilmutatud muutujaga

        public void SetSugu(bool sugu)
        {
            this.Sugu = sugu;
        }

        public void DeleteSugu()
        {
            this.Sugu = null;
        }

        public String SuguString
        {
            get
            {
                return
                  Sugu.HasValue ?
                      (Sugu.Value ? "Mees" : "Naine")
                      : "tundmatu";
            }
            set
            {
                switch (value.ToLower().Substring(0, 1))
                {

                    case "m":
                        Sugu = true;
                        break;
                    case "n":
                    case "f":
                        Sugu = false;
                        break;
                    default:
                        Sugu = null;
                        break;

                }
            }
        }
        public void SetSugu(string sugu)
        {
            switch (sugu.ToLower().Substring(0,1))
            {
 
                case "m":
                    Sugu = true;
                    break;
                case "n":
                case "f":
                    Sugu = false;
                    break;
                default:
                    Sugu = null;
                    break;

            }
        }

        public override string ToString()
        {
            return $"Inimene nimega {Nimi} sündinud {this.SünniAeg.ToShortDateString()}";
        }

    }
}
